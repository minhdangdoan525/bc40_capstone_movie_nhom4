import { createSlice } from '@reduxjs/toolkit'

const initialState = {
isOpen:false,
url:""
}

const movieModalSlice = createSlice({
  name: "movieModal",
  initialState,
  reducers: {
    setOpenModal:(state,action)=>{
      
        state.isOpen=true
        state.url=action.payload
    },
    setCloseModal:(state,action)=>{
      
      state.isOpen=false
      state.url=""
    }
  }
});

export const {setCloseModal,setOpenModal} = movieModalSlice.actions

export default movieModalSlice.reducer