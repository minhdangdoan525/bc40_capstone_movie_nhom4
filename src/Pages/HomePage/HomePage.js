import React from "react";
import MovieList from "./MovieList/MovieList";
import CinemaTab from "./CinemaTab/CinemaTab";
import MovieModal from "../../Component/MovieModal/MovieModal";
import Carousel from "./Carousel/Carousel";

export default function HomePage() {
  return (
    <div id="main-bg">
      <Carousel />
      <MovieList />
      <CinemaTab />
      <MovieModal />
    </div>
  );
}
