import React, { useEffect, useState } from "react";
import { movieSer } from "../../../service/movieService";
import ItemMovie from "./ItemMovie";
import { useDispatch } from "react-redux";
import {
  setLoadingOff,
  setLoadingOn,
} from "../../../reduxToolKit/spinnerSlice";
import Spinner from "../../../Component/Spinner/Spinner";

export default function () {
  const [movies, setMovies] = useState([]);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLoadingOn());
    movieSer
      .getMovieList()
      .then((res) => {
        setMovies(res.data.content);
        dispatch(setLoadingOff());
      })
      .catch((err) => {
        dispatch(setLoadingOff());
        console.log(err);
      });
  }, []);
  return (
    <div id="movie-list" className="movie-list container mx-auto flex flex-wrap justify-center  ">
      {movies.map((movie) => {
        return <ItemMovie movie={movie} key={movie.maPhim} />;
      })}
      
    </div>
  );
}

// grid grid-col-1 md:grid-cols-2 xl:grid-cols-4 gap-4 place-content-center