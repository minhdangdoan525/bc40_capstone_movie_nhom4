import React from 'react'
import { Desktop, Mobile, Tablet } from '../../../Layout/Responsive'
import ItemMovieDesktop from './ItemMovieDesktop'
import ItemMovieTablet from './ItemMovieTablet'
import ItemMovieMobile from './ItemMovieMobile'

export default function ItemMovie(props) {
  
  return (

    <div>
      <Desktop>
        <ItemMovieDesktop movie={props.movie} key={props.movie.maPhim}/>
      </Desktop>
      <Tablet>
        <ItemMovieTablet movie={props.movie} key={props.movie.maPhim}/>
      </Tablet>
      <Mobile>
        <ItemMovieMobile movie={props.movie} key={props.movie.maPhim}/>
      </Mobile>
    </div>
  )
}
