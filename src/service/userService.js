import { https } from "./config"

export const userService ={
    postLogin: (loginForm)=>{
        return https.post("/api/QuanLyNguoiDung/DangNhap",loginForm)
    },
    postSignup: (signupForm) => {
      return https.post("api/QuanLyNguoiDung/DangKy", signupForm);
    },
}