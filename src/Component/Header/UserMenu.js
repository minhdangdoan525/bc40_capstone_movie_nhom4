import React from 'react'
import { Desktop, Mobile, Tablet } from '../../Layout/Responsive'
import UserMenuDesktop from './UserMenuDesktop'
import UserMenuTablet from './UserMenuTablet'
import UserMenuMobile from './UserMenuMobile'

export default function UserMenu() {
  return (
    <div>
      <Desktop>
        <UserMenuDesktop/>
      </Desktop>
      <Tablet>
        <UserMenuTablet/>
      </Tablet>
      <Mobile>
        <UserMenuMobile/>
      </Mobile>
    </div>
  )
}
