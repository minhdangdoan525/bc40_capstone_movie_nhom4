import React from "react";
import { DownOutlined } from "@ant-design/icons";
import { Button, Dropdown, Space } from "antd";
import { useSelector } from "react-redux";
import { localStore } from "../../service/localUserStorage";
import { NavLink } from "react-router-dom";
import SearchBar from "./SearchBar";

const UserMenuDesktop = () => {
  let userInfor = useSelector((state) => {
    return state.userSlice.userInfor;
  });

  let handleLogout = () => {
    localStore.remove();
    window.location.reload();
    // window.location.href = "/login";
  };
  let renderMenu = () => {
    let buttonCss = "px-5 py-2 border-2 border-black";
    if (userInfor) {
      return (
        <div className="flex justify-between items-center gap-5">
          <SearchBar/>
          <Dropdown
            menu={{
              items: [
                { label: <span>Detail </span>, key: "0" },
                { label: <button onClick={handleLogout}>Logout</button> },
              ],
            }}
            trigger={["click"]}
          >
            <a onClick={(e) => e.preventDefault()}>
              <Space>
                {userInfor.hoTen}
                <DownOutlined />
              </Space>
            </a>
          </Dropdown>
        </div>
      );
    } else {
      return (
        <>
          <div className="flex justify-between items-center gap-5">
            <SearchBar/>

            <h5 className="text-right">
              Don’t have account yet ?
              <br />
              <NavLink to={"/sign-up"} className="text-right" >
                Please sign up here!
              </NavLink>
            </h5>
            <NavLink to={"/login"}>
              <button
                className={buttonCss}
                style={{
                  background:
                    "linear-gradient(154.49deg, #9580FF 15.16%, rgba(149, 128, 255, 0.39) 83.85%)",
                  borderRadius: "16px",
                }}
              >
                Login
              </button>
            </NavLink>
          </div>
        </>
      );
    }
  };
  return renderMenu();
};

export default UserMenuDesktop;
